<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['prefix' => 'v1', 'namespace' => 'API'], function () {
  // Login
  Route::post('/login','AuthController@postLogin');

  // Register
  Route::post('/register','AuthController@postRegister');

  // Logout
  Route::post('/logout','AuthController@postLogout');

  // Refresh Token
  Route::post('/refreshToken','AuthController@postRefreshToken');


  // Protected with APIToken Middleware
  Route::group(['middleware' => 'APIToken'], function () {

    Route::resource('/best-route','BestRouteController')
      ->only(['index', 'store', 'show', 'update', 'destroy']);

      // Refresh Token
    Route::post('/add-permission','AuthController@addPermission')->name('userAuth.addpermission');

  });


});

