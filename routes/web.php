<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Str;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/test', function() {
    $token = Str::random(64);
    return response()->json([
        'token' => $token,
        'len' => strlen($token)
    ]);
});

// Route::group(['prefix' => '/testing'], function() {
//     Route::get('/form', 'TestingController@form');
// });

// https://codebriefly.com/laravel-5-6-custom-token-base-api-authentication/
