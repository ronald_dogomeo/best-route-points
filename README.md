==================================================================================

Instructions:

	Before using this app, make sure you`ve imported the database dump.

	location: ___imported_database > route_point.sql
	database_name: route_point

==================================================================================


Guides:

----------------- User Register --------------------------

url: [POST method] http://localhost:8000/api/v1/register
data to be passed (json format):
{
	"name": "user2 dogomeo",
	"email": "user2@gmail.com",
	"password": "123456789",
	"role": "user"
}


==========================================================


----------------- User Login --------------------------

url: [POST method] http://localhost:8000/api/v1/login
data to be passed (json format):
{
	"email":"user2@gmail.com",
	"password": "123456789"
}


==========================================================


----------------- User Logout --------------------------

url: [POST method] http://localhost:8000/api/v1/logout

header Request
    Authorization: Bearer <token_here>
    
data to be passed (json format)
{
	"email":"user2@gmail.com",
	"password": "123456789"
}


============================================================

----------------- Add User Permission --------------------------

url: [POST method] http://localhost:8000/api/v1/add-permission

header Request
    Authorization: Bearer <token_here>

data to be passed (json format):
{
	"email": "user2@gmail.com",
	"permission": [
		"add", "edit", "delete"
	]
}


==========================================================

----------------- Add Route --------------------------

url: [POST method] http://localhost:8000/api/v1/best-route/

header Request
    Authorization: Bearer <token_here>
    
data to be passed (json format)
{
	"origin": {
		"point": "A",
		"time": "1",
		"cost": "20"
	},
	"routing_point": [
		{
			"point": "C",
			"time": "1",
			"cost": "12"
		}
	],
	"destination": "B"
}


============================================================

----------------- Get Possible Route --------------------------

url: [GET method] http://localhost:8000/api/v1/best-route/

header Request
    Authorization: Bearer <token_here>
    
data to be passed (json format)
{
	"origin": "A",
	"destination": "B"
}


============================================================


----------------- Get Best Route --------------------------

url: [GET method] http://localhost:8000/api/v1/best-route/a-b

header Request
    Authorization: Bearer <token_here>
    
data to be passed (json format)
{
	"origin": "A",
	"destination": "B",
	"lowest": "cost"
}


============================================================


----------------- Update Route --------------------------

url: [PUT method] http://localhost:8000/api/v1/best-route/edit-route

header Request
    Authorization: Bearer <token_here>
    
data to be passed (json format)
{
	"routeId": "56",
	"origin": {
		"point": "a",
		"time": "1",
		"cost": "20"
	},
	"routing_point": [
		{
			"point": "c",
			"time": "1",
			"cost": "12"
		}
	],
	"destination": "b"
}


============================================================


----------------- Delete Route --------------------------

url: [DELETE method] http://localhost:8000/api/v1/best-route/<route_id_here>

header Request
    Authorization: Bearer <token_here>
    
data to be passed (json format)
{}


============================================================

