-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 17, 2020 at 08:08 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.3.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `route_point`
--

-- --------------------------------------------------------

--
-- Table structure for table `bestroutes`
--

CREATE TABLE `bestroutes` (
  `id` int(10) UNSIGNED NOT NULL,
  `route_origin` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `route_time` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `route_cost` decimal(6,2) NOT NULL,
  `route_path` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`route_path`)),
  `route_destination` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bestroutes`
--

INSERT INTO `bestroutes` (`id`, `route_origin`, `route_time`, `route_cost`, `route_path`, `route_destination`, `user_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(17, 'A', '1', '20.00', '[{\"point\":\"C\",\"time\":\"1\",\"cost\":\"12\"}]', 'B', 1, NULL, '2020-04-16 06:28:36', '2020-04-16 06:28:36'),
(18, 'A', '30', '5.00', '[{\"point\":\"E\",\"time\":\"3\",\"cost\":\"5\"}]', 'D', 1, NULL, '2020-04-16 06:30:26', '2020-04-16 06:30:26'),
(19, 'A', '10', '1.00', '[{\"point\":\"H\",\"time\":\"30\",\"cost\":\"1\"}]', 'E', 1, NULL, '2020-04-16 06:31:01', '2020-04-16 06:31:01'),
(20, 'A', '10', '1.00', '[{\"point\":\"H\",\"time\":\"30\",\"cost\":\"1\"},{\"point\":\"E\",\"time\":\"3\",\"cost\":\"5\"}]', 'D', 1, NULL, '2020-04-16 06:31:58', '2020-04-16 06:31:58'),
(21, 'A', '10', '1.00', '[{\"point\":\"H\",\"time\":\"30\",\"cost\":\"1\"},{\"point\":\"E\",\"time\":\"3\",\"cost\":\"5\"},{\"point\":\"D\",\"time\":\"4\",\"cost\":\"50\"}]', 'F', 1, NULL, '2020-04-16 06:32:41', '2020-04-16 06:32:41'),
(22, 'A', '30', '5.00', '[{\"point\":\"E\",\"time\":\"3\",\"cost\":\"5\"},{\"point\":\"D\",\"time\":\"4\",\"cost\":\"50\"}]', 'F', 1, NULL, '2020-04-16 06:33:02', '2020-04-16 06:33:02'),
(23, 'A', '30', '5.00', '[{\"point\":\"E\",\"time\":\"3\",\"cost\":\"5\"},{\"point\":\"D\",\"time\":\"4\",\"cost\":\"50\"},{\"point\":\"F\",\"time\":\"45\",\"cost\":\"50\"}]', 'I', 1, NULL, '2020-04-16 06:33:32', '2020-04-16 06:33:32'),
(24, 'A', '10', '1.00', '[{\"point\":\"H\",\"time\":\"30\",\"cost\":\"1\"},{\"point\":\"E\",\"time\":\"3\",\"cost\":\"5\"},{\"point\":\"D\",\"time\":\"4\",\"cost\":\"50\"},{\"point\":\"F\",\"time\":\"45\",\"cost\":\"50\"}]', 'I', 1, NULL, '2020-04-16 06:34:09', '2020-04-16 06:34:09'),
(25, 'A', '10', '1.00', '[{\"point\":\"H\",\"time\":\"30\",\"cost\":\"1\"},{\"point\":\"E\",\"time\":\"3\",\"cost\":\"5\"},{\"point\":\"D\",\"time\":\"4\",\"cost\":\"50\"},{\"point\":\"F\",\"time\":\"40\",\"cost\":\"50\"}]', 'G', 1, NULL, '2020-04-16 06:35:15', '2020-04-16 06:35:15'),
(26, 'A', '30', '5.00', '[{\"point\":\"E\",\"time\":\"3\",\"cost\":\"5\"},{\"point\":\"D\",\"time\":\"4\",\"cost\":\"50\"},{\"point\":\"F\",\"time\":\"40\",\"cost\":\"50\"}]', 'G', 1, NULL, '2020-04-16 06:35:29', '2020-04-16 06:35:29'),
(27, 'A', '30', '5.00', '[{\"point\":\"E\",\"time\":\"3\",\"cost\":\"5\"},{\"point\":\"D\",\"time\":\"4\",\"cost\":\"50\"},{\"point\":\"F\",\"time\":\"45\",\"cost\":\"50\"},{\"point\":\"I\",\"time\":\"65\",\"cost\":\"5\"}]', 'B', 1, NULL, '2020-04-16 06:35:53', '2020-04-16 06:35:53'),
(29, 'A', '10', '1.00', '[{\"point\":\"H\",\"time\":\"30\",\"cost\":\"1\"},{\"point\":\"E\",\"time\":\"3\",\"cost\":\"5\"},{\"point\":\"D\",\"time\":\"4\",\"cost\":\"50\"},{\"point\":\"F\",\"time\":\"45\",\"cost\":\"50\"},{\"point\":\"I\",\"time\":\"65\",\"cost\":\"5\"}]', 'B', 1, NULL, '2020-04-16 06:37:11', '2020-04-16 06:37:11'),
(30, 'A', '10', '1.00', '[{\"point\":\"H\",\"time\":\"30\",\"cost\":\"1\"},{\"point\":\"E\",\"time\":\"3\",\"cost\":\"5\"},{\"point\":\"D\",\"time\":\"4\",\"cost\":\"50\"},{\"point\":\"F\",\"time\":\"40\",\"cost\":\"50\"},{\"point\":\"G\",\"time\":\"64\",\"cost\":\"73\"}]', 'B', 1, NULL, '2020-04-16 06:37:38', '2020-04-16 06:37:38'),
(31, 'A', '30', '5.00', '[{\"point\":\"E\",\"time\":\"3\",\"cost\":\"5\"},{\"point\":\"D\",\"time\":\"4\",\"cost\":\"50\"},{\"point\":\"F\",\"time\":\"40\",\"cost\":\"50\"},{\"point\":\"G\",\"time\":\"64\",\"cost\":\"73\"}]', 'B', 1, NULL, '2020-04-16 06:37:53', '2020-04-16 06:37:53'),
(32, 'H', '30', '1.00', '[{\"point\":\"E\",\"time\":\"3\",\"cost\":\"5\"},{\"point\":\"D\",\"time\":\"4\",\"cost\":\"50\"},{\"point\":\"F\",\"time\":\"40\",\"cost\":\"50\"},{\"point\":\"G\",\"time\":\"64\",\"cost\":\"73\"}]', 'B', 1, NULL, '2020-04-16 06:47:28', '2020-04-16 06:47:28'),
(33, 'H', '30', '1.00', '[{\"point\":\"E\",\"time\":\"3\",\"cost\":\"5\"},{\"point\":\"D\",\"time\":\"4\",\"cost\":\"50\"},{\"point\":\"F\",\"time\":\"45\",\"cost\":\"50\"},{\"point\":\"I\",\"time\":\"65\",\"cost\":\"5\"}]', 'B', 1, NULL, '2020-04-16 06:47:47', '2020-04-16 06:47:47'),
(34, 'E', '3', '5.00', '[{\"point\":\"D\",\"time\":\"4\",\"cost\":\"50\"},{\"point\":\"F\",\"time\":\"45\",\"cost\":\"50\"},{\"point\":\"I\",\"time\":\"65\",\"cost\":\"75\"}]', 'B', 1, NULL, '2020-04-16 06:48:09', '2020-04-16 06:48:09'),
(35, 'E', '3', '5.00', '[{\"point\":\"D\",\"time\":\"4\",\"cost\":\"50\"},{\"point\":\"F\",\"time\":\"40\",\"cost\":\"50\"},{\"point\":\"G\",\"time\":\"64\",\"cost\":\"73\"}]', 'B', 1, NULL, '2020-04-16 06:49:59', '2020-04-16 06:49:59'),
(36, 'D', '4', '50.00', '[{\"point\":\"F\",\"time\":\"40\",\"cost\":\"50\"},{\"point\":\"G\",\"time\":\"64\",\"cost\":\"73\"}]', 'B', 1, NULL, '2020-04-16 06:51:07', '2020-04-16 06:51:07'),
(37, 'D', '4', '50.00', '[{\"point\":\"F\",\"time\":\"45\",\"cost\":\"50\"},{\"point\":\"I\",\"time\":\"65\",\"cost\":\"5\"}]', 'B', 1, NULL, '2020-04-16 06:51:24', '2020-04-16 06:51:24'),
(38, 'F', '45', '50.00', '[{\"point\":\"I\",\"time\":\"65\",\"cost\":\"5\"}]', 'B', 1, NULL, '2020-04-16 06:51:45', '2020-04-16 06:51:45'),
(39, 'F', '40', '50.00', '[{\"point\":\"G\",\"time\":\"64\",\"cost\":\"73\"}]', 'B', 1, NULL, '2020-04-16 06:52:01', '2020-04-16 06:52:01'),
(40, 'E', '3', '5.00', '[{\"point\":\"D\",\"time\":\"4\",\"cost\":\"50\"}]', 'F', 1, NULL, '2020-04-16 06:52:47', '2020-04-16 06:52:47'),
(41, 'H', '30', '1.00', '[{\"point\":\"E\",\"time\":\"3\",\"cost\":\"5\"}]', 'D', 1, NULL, '2020-04-16 06:55:03', '2020-04-16 06:55:03'),
(43, 'D', '4', '50.00', '[{\"point\":\"F\",\"time\":\"45\",\"cost\":\"50\"}]', 'I', 1, NULL, '2020-04-16 07:00:11', '2020-04-16 07:00:11'),
(45, 'D', '4', '50.00', '[{\"point\":\"F\",\"time\":\"40\",\"cost\":\"50\"}]', 'G', 1, NULL, '2020-04-16 07:09:14', '2020-04-16 07:09:14'),
(46, 'H', '30', '1.00', '[{\"point\":\"E\",\"time\":\"3\",\"cost\":\"5\"},{\"point\":\"D\",\"time\":\"4\",\"cost\":\"50\"}]', 'F', 1, NULL, '2020-04-16 07:10:08', '2020-04-16 07:10:08'),
(47, 'H', '30', '1.00', '[{\"point\":\"E\",\"time\":\"3\",\"cost\":\"5\"},{\"point\":\"D\",\"time\":\"4\",\"cost\":\"50\"},{\"point\":\"F\",\"time\":\"40\",\"cost\":\"50\"}]', 'G', 1, NULL, '2020-04-16 07:11:17', '2020-04-16 07:11:17'),
(48, 'H', '30', '1.00', '[{\"point\":\"E\",\"time\":\"3\",\"cost\":\"5\"},{\"point\":\"D\",\"time\":\"4\",\"cost\":\"50\"},{\"point\":\"F\",\"time\":\"45\",\"cost\":\"50\"}]', 'I', 1, NULL, '2020-04-16 07:11:29', '2020-04-16 07:11:29'),
(49, 'E', '3', '5.00', '[{\"point\":\"D\",\"time\":\"4\",\"cost\":\"50\"},{\"point\":\"F\",\"time\":\"45\",\"cost\":\"50\"}]', 'I', 1, NULL, '2020-04-16 07:11:46', '2020-04-16 07:11:46');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2014_10_12_100001_create_tokens_table', 1),
(4, '2020_04_13_044505_create_permissions_table', 1),
(5, '2020_04_15_120130_create_routepoints_table', 1),
(6, '2020_04_16_083840_create_bestroute_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `permission`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'add', 1, '2020-04-15 04:17:42', '2020-04-15 04:17:42'),
(2, 'edit', 1, '2020-04-15 04:17:42', '2020-04-15 04:17:42'),
(3, 'delete', 1, '2020-04-15 04:17:42', '2020-04-15 04:17:42'),
(4, 'view', 1, '2020-04-15 04:17:42', '2020-04-15 04:17:42'),
(5, 'add', 3, '2020-04-16 07:56:45', '2020-04-16 07:56:45'),
(7, 'delete', 3, '2020-04-16 07:56:45', '2020-04-16 07:56:45'),
(9, 'edit', 3, '2020-04-16 08:39:03', '2020-04-16 08:39:03');

-- --------------------------------------------------------

--
-- Table structure for table `tokens`
--

CREATE TABLE `tokens` (
  `token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `refreshToken` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tokens`
--

INSERT INTO `tokens` (`token`, `refreshToken`, `user_id`, `created_at`, `updated_at`) VALUES
('RSgdJQZQNxhfYSTJN8CVd7tHQgtO774WYT5OaXX7tLSnOPM6uuKRMtgi15m5DtIv5e96fb663d272', '1lgqNpyL93Yyxyg0kaNtekmBNP40M8KPQFwoylIUvrC1wCKS8KrOiCxuimxVqD105e96fb663d277', 1, '2020-04-15 04:17:42', '2020-04-15 04:17:42'),
('TpUQRgdqb273EBBRhwX9GYUP3KDbV75Mpzkdbf8LmqaIiwKHvvCejd46fGNOeJzC5e987d5b4bc37', '50ixE2ubxp29YRi6ddkRXaE7YHlde80aTqBAIw8xSvUmA4vbp67pJW7koT3zgw5u5e987d5b4bc3f', 2, '2020-04-16 07:44:27', '2020-04-16 07:44:27'),
('Kj3b1U3UWJZiobAXN8bZzS5QY8BJ01NrSTIxnJqM5wKimmnz0JuZNgPksheLhlyd5e987d632897e', 'Mm75zwEYK5btBqrz7uqdaRJePTZj4WbjK9RvBmdp1rxuht22ig98evuDikKhGBUr5e987d6328988', 3, '2020-04-16 07:44:35', '2020-04-16 07:44:35'),
('grKrwbgsJpV00sXan5TKzsXAJzb5vFJGvOaFn7Vfc8WXtGlODEDH6aEnk7uIqXaJ5e987db2ad3d2', 'VO1fC8osXPkKVvF9c9CPl57yXXj2Q9tq4yAwWuF04q8grW3Nl0ITqw6vDm0dJksr5e987db2ad3da', 1, '2020-04-16 07:45:54', '2020-04-16 07:45:54');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` enum('user','admin') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin dogomeo', 'admin@gmail.com', '$2y$10$LjJlO.1Gh18LZB6NneOR.eXT1gKpSS5y0BSF9AoPHNtoe/taRil7O', 'admin', NULL, '2020-04-15 04:17:42', '2020-04-15 04:17:42'),
(2, 'user1 dogomeo', 'user1@gmail.com', '$2y$10$kxoCju9vU60.pr9Okh/yn..wyRKUzBn8DW/8ZNjSuxF2Ui2Bjms9u', 'user', NULL, '2020-04-16 07:44:27', '2020-04-16 07:44:27'),
(3, 'user2 dogomeo', 'user2@gmail.com', '$2y$10$BsONK6PYvoVsmUKCzYfT7u6Et2sVqJJnV1hlwvK2bdVHDCZRQ4DEG', 'user', NULL, '2020-04-16 07:44:35', '2020-04-16 07:44:35');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bestroutes`
--
ALTER TABLE `bestroutes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bestroutes_user_id_index` (`user_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_user_id_index` (`user_id`);

--
-- Indexes for table `tokens`
--
ALTER TABLE `tokens`
  ADD UNIQUE KEY `tokens_token_unique` (`token`),
  ADD UNIQUE KEY `tokens_refreshtoken_unique` (`refreshToken`),
  ADD KEY `tokens_user_id_index` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bestroutes`
--
ALTER TABLE `bestroutes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `bestroutes`
--
ALTER TABLE `bestroutes`
  ADD CONSTRAINT `bestroutes_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `permissions`
--
ALTER TABLE `permissions`
  ADD CONSTRAINT `permissions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tokens`
--
ALTER TABLE `tokens`
  ADD CONSTRAINT `tokens_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
