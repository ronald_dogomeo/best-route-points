<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Carbon;
use DB;
use App\User;
use App\Token;
use App\Permission;
use Validator;

class AuthController extends Controller {
  private $apiToken;

  public function __construct() {
    // Unique Token
    $this->apiToken = uniqid(Str::random(64));
  }
  /**
   * Client Login
   */
  public function postLogin(Request $request) {
    // Validations
    $rules = [
      'email'=>'required|email',
      'password'=>'required|min:8'
    ];
    $validator = Validator::make($request->all(), $rules);
    if ($validator->fails()) {
      // Validation failed
      return response()->json([
        'message' => $validator->messages(),
      ]);
    } else {

      $credentials = $request->only('email', 'password');

      if (Auth::attempt($credentials)) {
        $user = Auth::user();
        $lastToken = $user->tokens->last();
        
        if ( $lastToken->created_at < Carbon::now()->subMinutes(env('TOKEN_EXPIRE_SEC', 1)) ) {
          if (!$lastToken = $this->refreshToken($lastToken)) {
            return response()->json([
              'message'   =>  'Invalid Token',
            ]);
          }
        }

        return response()->json([
          'response'   =>  $this->getToken($lastToken),
        ]);

      } else {
        return response()->json([
          'message' => 'User not found',
        ]);
      }

    }
  }

  /**
   * Register
   */
  public function postRegister(Request $request)
  {
    // Validations
    $rules = [
      'name'     => 'required|min:3',
      'email'    => 'required|unique:users,email',
      'password' => 'required|min:8'
    ];
    
    $validator = Validator::make($request->all(), $rules);
    if ($validator->fails()) {
      // Validation failed
      return response()->json([
        'message' => $validator->messages(),
      ]);
    } else {

      $postArray = [
        'name'      => $request->name,
        'email'     => $request->email,
        'password'  => bcrypt($request->password),
      ];
      // $user = User::GetInsertId($postArray);
      $user = User::create($postArray);
      if (isset($request->role) && $request->role == 'admin') {
        
        $user->role = 'admin';
        $user->save();

        $permit = array(
          new Permission(['permission' => 'add']),
          new Permission(['permission' => 'edit']),
          new Permission(['permission' => 'delete']),
          new Permission(['permission' => 'view']),
        );

        $user->permissions()->saveMany($permit);
        
      }
      $token = new Token(['token' => uniqid(Str::random(64)), 'refreshToken' => uniqid(Str::random(64))]);
      $user->tokens()->save($token);
  
      if( $user && $data = $this->getToken($token) ) {
        return response()->json([
          'response' => $data
        ]);
      } else {
        return response()->json([
          'message' => 'Registration failed, please try again.',
        ]);
      }

    }
  }

  public function getToken ($token) {
    $activeToken = Token::where([
      ['token', $token->token]
    ])->first()->setHidden(['created_at', 'updated_at', 'user_id', 'user']);

    if ( !$activeToken ) return false;

    $user = $activeToken->user->setHidden(['created_at', 'updated_at', 'id', 'remember_token', 'role', 'password']);

    $data = array();

    foreach(json_decode($user, true) as $key => $value) {
      $data[$key] = $value;
    }

    foreach(json_decode($activeToken, true) as $key => $tkn) {
      $data[$key] = $tkn;
    }

    return $data;
  }


  public function refreshToken ($token) {

    $oldToken = Token::where('token', $token->token)->first();

    if ($oldToken->refreshToken == $token->refreshToken) {
      $user = $oldToken->user;
      $newToken = new Token(['token' => uniqid(Str::random(64)), 'refreshToken' => uniqid(Str::random(64))]);
      $user->tokens()->save( $newToken );

      return $newToken;

    }

    return false;

  }

  /**
   * Logout
   */
  public function postLogout(Request $request)
  {

    $token = str_replace('Bearer ', '', $request->header('Authorization'));

    $activeToken = Token::where('token', $token)->first();
 
    $msg = null;
    $name = null;
    $email = null;

    if($activeToken) {
      
      return response()->json([
        'response' => [
          'message' => 'User log out',
          'name' => $activeToken->user->name,
          'email' => $activeToken->user->email,
          'token' => null,
          'refreshToken' => null,
        ]
      ]);

    } else {

      return response()->json([
        'message' => 'User not found!'
      ]);

    }

  }

  // Refreshing Token
  public function postRefreshToken (Request $request) {
    $header = $request->header('Authorization');
    if (Str::startsWith($header, 'Bearer ')) {
      //return Str::substr($header, 7);
      $token = str_replace( 'Bearer ', '', $header );
      $refreshToken = $request->header('refreshToken');

      $response = $this->refreshToken( (object) ['token' => $token, 'refreshToken' => $refreshToken] );
      return response()->json([
        'response' => $response->setHidden(['id', 'user_id', 'updated_at', 'created_at']),
      ]);
    }

    return response()->json([
      'message' => 'Invalid Token',
    ]);
    
  }

  public function addPermission( Request $request ) {

    
    $user = User::where('email', $request->email)->first();

    if ( $user->role == 'admin' ) {
      return response()->json([
        'message' => 'Cannot add permission from admin!',
      ]);
    }

    $permit = [];

    $tempRule = [];

    foreach ( $request->permission as $key => $rule ) {
      $tempRule[] = $rule;
      $perm = Permission::where([
        ['permission', $rule],
        ['user_id', $user->id]
      ])->first();

      if ( !$perm ) {
        $permit[] = new Permission(['permission' => $rule]);
      }
    }

    $user->permissions()->saveMany($permit);
    
    
    return response()->json([
      'message' => 'Permission ['.implode( ', ', $tempRule ).'] added to '. ucwords($user->name),
    ]);

  }

}