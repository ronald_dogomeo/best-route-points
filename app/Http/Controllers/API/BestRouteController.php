<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\Bestroute;

class BestRouteController extends Controller
{

    public function __construct() {
        $this->middleware('Permission');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {
        
        $routes = Bestroute::where([
            ['route_origin', strtoupper( $request->origin )],
            ['route_destination', strtoupper( $request->destination )]
        ])->get();

        $points = [];

        foreach( $routes as $item ) {
            $route = $item->route_origin;
            $cost = $item->route_cost;
            $time = $item->route_time;

            foreach ( json_decode( $item->route_path, true ) as $key => $point) {
                $route .= ' -> '.$point['point'];
                $cost += $point['cost'];
                $time += $point['time'];
            }
            $route .= ' -> '.$item->route_destination;
            $points[] = [
                'point' => $route,
                'time' => $time,
                'cost' => $cost
            ];
            $item->setHidden(['id', 'user_id', 'deleted_at', 'created_at', 'updated_at']);
            $item->route_path = json_decode( $item->route_path, true );
        }


        return response()->json( $points );

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function isExist( $from, $to, $path ) {
        $isExists = Bestroute::where([
            ['route_origin', strtoupper( $from )],
            ['route_path', $path],
            ['route_destination', strtoupper( $to )]
        ])->first();

        return $isExists;
    }

    public function store(Request $request)
    {

        $routing_point = array_map(function ($val) {
            $val['point'] = strtoupper($val['point']);
            return $val;
        }, $request->routing_point);

        
        if ( $this->isExist( $request->origin['point'], $request->destination, json_encode( $routing_point ) ) ) {
            return response()->json([
                'message' => 'Route points existed!',
            ]);
        }

        $routing_point = array_map(function ($val) {
            $val['point'] = strtoupper($val['point']);
            return $val;
        }, $request->routing_point);

        $points = new Bestroute([
            'route_origin'=> strtoupper( $request->origin['point'] ),
            'route_destination' => strtoupper( $request->destination ),
            'route_path' => json_encode( $routing_point ),
            'route_time' => $request->origin['time'],
            'route_cost' => $request->origin['cost'],
        ]);

        $request->user->bestroutes()->save($points); 

        return response()->json([
            'message' => 'Route points created!',
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show(Request $request)
    {
        $routes = Bestroute::where([
            ['route_origin', strtoupper( $request->origin )],
            ['route_destination', strtoupper( $request->destination )]
        ])->get();

        $points = [];


        $minTime = 1000000000;
        $minCost = 1000000000;

        $bestRoute = [];

        foreach( $routes as $item ) {
            $route = $item->route_origin;
            $cost = $item->route_cost;
            $time = $item->route_time;

            foreach ( json_decode( $item->route_path, true ) as $key => $point) {
                $route .= ' -> '.$point['point'];
                $cost += $point['cost'];
                $time += $point['time'];
            }
            $route .= ' -> '.$item->route_destination;
            $obj = [
                'point' => $route,
                'time' => $time,
                'cost' => $cost
            ];
            $points[] = $obj;

            if ($minTime > $time && $request->lowest == 'time') {
                $minTime = $time;
                $bestRoute = $obj;
            }

            if ($minCost > $cost && $request->lowest == 'cost' ) {
                $minCost = $cost;
                $bestRoute = $obj;
            }

            $item->setHidden(['id', 'user_id', 'deleted_at', 'created_at', 'updated_at']);
            $item->route_path = json_decode( $item->route_path, true );
        }


        return response()->json( $bestRoute );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function checkRoute ( $request ) {
        
        $route = Bestroute::where([
            [ 'route_origin', strtoupper( $request->origin['point'] ) ],
            [ 'route_destination', strtoupper( $request->destination) ]
        ])->get();

        if ( $route->count() > 0 ) {
            $isMatch = [];
            foreach ( $route as $key => $path ) {
                $decoded = json_decode( $path->route_path, true );
                if ( count($request->routing_point) == count( $decoded ) ) {
                    for ( $ind = 0; $ind < count($decoded); $ind ++  ) {
                        if ( $decoded[$ind]['point'] == strtoupper( $request->routing_point[$ind]['point'] ) ) {
                            $isMatch[] = true;
                        } else {
                            $isMatch[] = false;
                        }
                    }
                    
                }
            }

            if ( in_array( false, $isMatch ) ) {
                return 'available';
            }
            
            return 'exists';
        }

        return 'available';
    }
    public function update(Request $request, $id)
    {

        $route = Bestroute::find( $request->routeId );

        if ( !$route ) {
            return response()->json([
                'message' => 'Route not found!',
            ]);
        }

        if ( $request->origin ) {
            $route->route_origin = strtoupper( $request->origin['point'] );
        }

        if ( $request->origin['time'] ) {
            $route->route_time = $request->origin['time'];
        }

        if ( $request->origin['cost'] ) {
            $route->route_cost = $request->origin['cost'];
        }

        if ( $request->destination ) {
            $route->route_destination = strtoupper( $request->destination );
        }

        if ( $request->routing_point ) {
            $routing_point = array_map(function ($val) {
                $val['point'] = strtoupper($val['point']);
                return $val;
            }, $request->routing_point);

            $route->route_path = json_encode( $routing_point );
        }

        $isExist = Bestroute::where( [
            ['route_origin', $route->route_origin],
            ['route_path', $route->route_path],
            ['route_destination', $route->route_destination],
        ] )->first();

        if ( $isExist ) {
            return response()->json([
                'message' => 'Duplicate: Cannot update route!',
            ]);
        }

        // $isAvail = $this->checkRoute( $request );

        // if ( $isAvail == 'exists' ) {
        //     return response()->json([
        //         'message' => 'Duplicate: Cannot update route!',
        //     ]);
        // }

        $route->save();

        $route['route_path'] = json_decode( $route['route_path'], true );

        return response()->json( $route );
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $route = Bestroute::find( $id );

        if ( !$route ) {
            return response()->json([
                'message' => 'Route not found!',
            ]);
        }

        $route->delete();

        return response()->json([
            'message' => 'Route deleted',
        ]);
    }
}
