<?php

namespace App\Http\Middleware;

use Closure;
use App\Token;

class UserPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = str_replace( 'Bearer ', '', $request->header('Authorization') );
        
        $user = Token::where('token', $token)->first();

        $request->user = $user->user;

        if ($request->user->role == 'admin') {
            return response()->json([
                'message' => 'Admin: To order the item/s is only availble for users'
            ]);
        }
        
        return $next($request);
    }
}
