<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use App\Token;

class HasPermission {

    private $permission;

    public function __construct($request) {
        $token = str_replace( 'Bearer ', '', $request->header('Authorization') );
        
        $user = Token::where('token', $token)->first();

        $this->permission = $user->user->permissions;
    }

    public function can($param) {
        
        return $this->permission->where('permission', $param)->count();
    }
}

class Permission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle( $request, Closure $next )
    {

        $permission = new HasPermission($request);

        $route = explode('.', $request->route()->getName());

        $rule = [
            'create' => 'add',
            'store' => 'add',
            'edit' => 'edit',
            'update' => 'edit',
            'show' => 'view',
            'index' => 'view',
            'destroy' => 'delete',
        ];

        if ( $isPermitted = $permission->can( $rule[$route[1]] ) || $rule[$route[1]] == 'view') {
            return $next($request);
        }

        return response()->json([
            'message' => 'no permission to '. $rule[$route[1]],
        ]);
    }
}
