<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Carbon;
use Closure;
use App\User;
use App\Token;

class APIToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if ($bearer = $this->bearerToken($request)) {
            $token = Token::where('token', $bearer)->first();

            if ( !$token ) {
                return response()->json([
                    'message' => 'Invalid Token',
                ]);
            }

            if ( !$token->created_at > Carbon::now()->subMinutes(env('TOKEN_EXPIRE_MIN', 1)) ) {
                return response()->json([
                    'message' => 'Token expired',
                ]);
            }

            $request->user = $token->user;

            $route = explode('.', $request->route()->getName());

            if ( $token->user->role == 'user' && $route[0] == 'userAuth' && $route[1] == 'addpermission') {
                return response()->json([
                    'message' => 'You are not allowed here!',
                ]);
            }

            return $next($request);
        }

        return response()->json([
            'message' => 'Invalid Token',
        ]);

    }

    public function bearerToken($request) 
    {
        $header = $request->header('Authorization');
        if (Str::startsWith($header, 'Bearer ')) {
            return Str::substr($header, 7);
        }
        return false;
    }
}

