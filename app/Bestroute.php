<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bestroute extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'route_origin',
        'route_destination',
        'route_path',
        'route_cost',
        'route_time'
    ];

    public function user() {
        return $this->hasMany('App\User');
    }
}
