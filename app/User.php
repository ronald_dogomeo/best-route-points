<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'email', 
        'password',
        'role',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function tokens() {
        return $this->hasMany('App\Token');
    }

    public function permissions () {
        return $this->hasMany('App\Permission');
    }

    public function routepoints () {
        return $this->hasMany('App\Routepoint');
    }

    public function bestroutes () {
        return $this->hasMany('App\Bestroute');
    }

}
